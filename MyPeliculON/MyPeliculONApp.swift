//
//  MyPeliculONApp.swift
//  MyPeliculON
//
//  Created by David Novella on 29/3/22.
//

import SwiftUI

@main
struct MyPeliculONApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
